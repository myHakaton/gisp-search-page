import {Affix, Button, Divider, Drawer, Form, Input, Layout, List, Tree} from "antd";
import {
    CarryOutOutlined,
    DollarOutlined,
    EnvironmentOutlined,
    FundOutlined,
    MehOutlined,
    SettingOutlined,
    TeamOutlined,
    ThunderboltOutlined
} from "@ant-design/icons";
import {useEffect, useMemo, useState} from "react";
import axios from "axios";
import lodash from "lodash";

const data = [
    {
        key: 'region',
        avatar: <EnvironmentOutlined/>,
        title: 'Регион',
        description: "Тюменская область и еще 2"
    },
    {
        key: 'appliance',
        avatar: <ThunderboltOutlined/>,
        title: 'Отрасль',
        description: "Разработка ПО и цифровые платформы и еще 38"
    },
    {
        avatar: <TeamOutlined/>,
        title: ' Размер предприятия',
        description: "Средний (100 - 250 чел.) и еще 2"
    },
    {
        key: 'typeMera',
        avatar: <DollarOutlined/>,
        title: 'Тип меры поддержки',
        description: "Финансовая поддержка и еще 4"
    },
    {
        key: 'complexity',
        avatar: <MehOutlined/>,
        title: 'Проблемы предприятия',
        description: "Кадровые проблемы и еще 2 "
    },
    {
        avatar: <FundOutlined/>,
        title: 'Этап развития проекта/предприятия',
        description: "Идея (стартап) и еще 3"
    }
];

export const Setting = (props) => {
    const [show, setShow] = useState('');
    const [treeData, setTreeData] = useState([]);
    const [innValue, setInnValue] = useState('');
    const [searchInn, setSearchInn] = useState('');
    const onSearchValue = useMemo(() => {
        return lodash.debounce((value) => {
            setSearchInn(value);
        }, 1000);
    }, [
        props.visitorId
    ]);
    useEffect(()=>{
        console.log(props.user);
        if(props.user) {
            setInnValue(props.user.inn);
        }
    },[props.user])
    useEffect(() => {
        if (!props.visitorId || !searchInn) return
        axios.post('/api/updateByInn', {
            fingerprint: props.visitorId,
            inn: searchInn
        }).then(response => {
            const fingerprint = response.data.fingerprint;
            props.setVisitorId(undefined);
            return axios.post('/api/getUser', {
                fingerprint
            });
        }).then(response => {
            props.setUser(response.data);
            props.setVisitorId(response.data.fingerprint);
        });
    }, [searchInn])
    useEffect(() => {
        if (!show || !props.user) return
        const data = props.user.properties[show];
        setTreeData(lodash.sortBy(Object.entries(data).map(([key, index]) => ({
            title: key,
            key: index
        })), 'key'));
    }, [show]);
    return (
        <Layout.Sider style={{
            background: '#fff',
            padding: 8
        }} width={280}>
            <Affix offsetTop={10}>
                <Form.Item label="ИНН">
                    <Input placeholder={'ИНН'} value={innValue} onChange={(e) => {
                        setInnValue(e.target.value);
                        onSearchValue(e.target.value);
                    }}/>
                </Form.Item>
                <Divider/>
                <List
                    itemLayout="horizontal"
                    dataSource={data}
                    renderItem={item => {
                        let list = [];
                        if (item.key && props.user) {
                            const data = props.user.properties[item.key];
                            list = lodash.sortBy(Object.entries(data).map(([key, index]) => ({
                                title: key,
                                key: index
                            })), 'key');
                        }
                        return (
                            <List.Item extra={
                                <Button type={'link'}
                                        icon={<SettingOutlined/>}
                                        hidden={!item.key && props.user}
                                        onClick={() => {
                                            setShow(item.key);
                                        }}
                                />
                            }>
                                <List.Item.Meta
                                    avatar={item.avatar}
                                    title={item.title}
                                    description={list.length ? list.length > 1 ?
                                        `${list[0].title} и еще +${list.length - 1}`
                                        : `${list[0].title}` : ''
                                    }
                                />
                            </List.Item>
                        )
                    }}
                />
            </Affix>
            <Drawer title="Приоритеты"
                    width={350}
                    visible={show}
                    onClose={() => {
                        setShow('');
                    }}
            >
                <Tree
                    icon={<CarryOutOutlined/>}
                    showIcon
                    draggable
                    treeData={treeData}
                />
            </Drawer>
        </Layout.Sider>
    )
}