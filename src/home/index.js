import {Badge, Card, Col, Divider, Row, Space, Tag, Typography} from "antd";
import {DislikeOutlined, EllipsisOutlined, LikeOutlined, GiftOutlined} from '@ant-design/icons';
import {useEffect, useState} from "react";
import axios from "axios";
import lodash from "lodash";


export const HomePage = (props) => {
    const [recommendations, setRecommendations] = useState([]);
    const [mostPopularByRegion, setMostPopularByRegion] = useState([]);
    const [mostPopularByAplince, setMostPopularByAplince] = useState([]);
    const onLikesClick = async (id) => {
        await axios.post(`/api/view?gispId=${id}`, {
            fingerprint: props.visitorId
        });
        props.setVisitorId(undefined);
        const {data} = await axios.post('/api/getUser', {
            fingerprint: props.visitorId
        });
        props.setUser(data);
        props.setVisitorId(data.fingerprint);
    };
    const onDisLikesClick = async (id) => {
        await axios.post(`/api/dislike?gispId=${id}`, {
            fingerprint: props.visitorId
        });
        props.setVisitorId(undefined);
        const {data} = await axios.post('/api/getUser', {
            fingerprint: props.visitorId
        });
        props.setUser(data);
        props.setVisitorId(data.fingerprint);
    };
    useEffect(() => {
        if (!props.visitorId) return
        axios.post('/api/getRecommendations', {
            fingerprint: props.visitorId
        }).then(response => {
            setRecommendations(response.data);
        });
    }, [props.visitorId]);
    useEffect(() => {
        if (!props.visitorId) return
        axios.post('/api/getMostPopularByRegion', {
            fingerprint: props.visitorId
        }).then(response => {
            setMostPopularByRegion(response.data);
        });
    }, [props.visitorId]);
    useEffect(() => {
        if (!props.visitorId) return
        axios.post('/api/getMostPopularByAplince', {
            fingerprint: props.visitorId
        }).then(response => {
            setMostPopularByAplince(response.data);
        });
    }, [props.visitorId]);
    return (
        <div>
            <Divider orientation="left">
                <Typography.Title level={3}>
                    Рекомендации для вас
                </Typography.Title>
            </Divider>
            <Row gutter={16}>
                {lodash.sortBy(recommendations, (obj) => {
                    return obj.likes - obj.disLikes
                }).reverse().map(item => {
                    const tagList = item.applianceId.split(',');
                    return (
                        <Col span={8} style={{
                            height: 250
                        }}>
                            <Card className={'Slide'}
                                  actions={[
                                      <Badge key="historuCount"
                                             style={{backgroundColor: 'green'}}
                                             count={item.historuCount}
                                      />,
                                      <Space key="like">
                                          <LikeOutlined onClick={async ()=>{ await onLikesClick(item.id)}}/>
                                          <Badge count={item.likes} showZero/>
                                      </Space>,
                                      <Space key="dislike">
                                          <DislikeOutlined onClick={async ()=>{ await onDisLikesClick(item.id)}}/>
                                          <Badge count={item.disLikes} showZero/>
                                      </Space>,
                                      <EllipsisOutlined key="ellipsis" onClick={() => {
                                          window.open(`https://gisp.gov.ru/support-measures/list/${item.id}/`)
                                      }}/>
                                  ]}>
                                <Card.Meta title={item.typeMera} description={
                                    <>
                                        <Typography.Paragraph
                                            style={{color: '#9f6f5a'}}
                                            strong
                                            ellipsis={{
                                                rows: 3
                                            }}
                                        >
                                            {item.smallName}
                                        </Typography.Paragraph>
                                        <Typography.Paragraph
                                            ellipsis={{
                                                rows: 1
                                            }}
                                        >
                                            {tagList.map(item => (
                                                <Tag>{item}</Tag>
                                            ))}
                                        </Typography.Paragraph>
                                    </>
                                }/>
                            </Card>
                        </Col>
                    )
                })}
            </Row>
            <Divider orientation="left">
                <Typography.Title level={3}>
                    Популярные в вашем регионе
                </Typography.Title>
            </Divider>
            <Row gutter={16}>
                {lodash.sortBy(mostPopularByRegion, (obj) => {
                    return obj.likes - obj.disLikes
                }).reverse().map(item => {
                    const tagList = item.applianceId.split(',');
                    return (
                        <Col span={12} style={{
                            height: 250
                        }}>
                            <Card className={'Slide'}
                                  actions={[
                                      <Badge key="historuCount"
                                             style={{backgroundColor: 'green'}}
                                             count={item.historuCount}
                                      />,
                                      <Space key="like">
                                          <LikeOutlined onClick={async ()=>{ await onLikesClick(item.id)}}/>
                                          <Badge count={item.likes} showZero/>
                                      </Space>,
                                      <Space key="dislike">
                                          <DislikeOutlined onClick={async ()=>{ await onDisLikesClick(item.id)}}/>
                                          <Badge count={item.disLikes} showZero/>
                                      </Space>,
                                      <EllipsisOutlined key="ellipsis" onClick={() => {
                                          window.open(`https://gisp.gov.ru/support-measures/list/${item.id}/`)
                                      }}/>
                                  ]}>
                                <Card.Meta title={item.typeMera} description={
                                    <>
                                        <Typography.Paragraph
                                            style={{color: '#9f6f5a'}}
                                            strong
                                            ellipsis={{
                                                rows: 3
                                            }}
                                        >
                                            {item.smallName}
                                        </Typography.Paragraph>
                                        <Typography.Paragraph
                                            ellipsis={{
                                                rows: 1
                                            }}
                                        >
                                            {tagList.map(item => (
                                                <Tag>{item}</Tag>
                                            ))}
                                        </Typography.Paragraph>
                                    </>
                                }/>
                            </Card>
                        </Col>
                    )
                })}
            </Row>
            <Divider orientation="left">
                <Typography.Title level={3}>
                    Популярные в вашей отрасли
                </Typography.Title>
            </Divider>
            <Row gutter={16}>
                {lodash.sortBy(mostPopularByAplince, (obj) => {
                    return obj.likes - obj.disLikes
                }).reverse().map(item => {
                    const tagList = item.applianceId.split(',');
                    return (
                        <Col span={12} style={{
                            height: 250
                        }}>
                            <Card className={'Slide'}
                                  actions={[
                                      <Badge key="historuCount"
                                             style={{backgroundColor: 'green'}}
                                             count={item.historuCount}
                                      />,
                                      <Space key="like">
                                          <LikeOutlined onClick={async ()=>{ await onLikesClick(item.id)}}/>
                                          <Badge count={item.likes} showZero/>
                                      </Space>,
                                      <Space key="dislike">
                                          <DislikeOutlined onClick={async ()=>{ await onDisLikesClick(item.id)}}/>
                                          <Badge count={item.disLikes} showZero/>
                                      </Space>,
                                      <EllipsisOutlined key="ellipsis" onClick={() => {
                                          window.open(`https://gisp.gov.ru/support-measures/list/${item.id}/`)
                                      }}/>
                                  ]}>
                                <Card.Meta title={item.typeMera} description={
                                    <>
                                        <Typography.Paragraph
                                            style={{color: '#9f6f5a'}}
                                            strong
                                            ellipsis={{
                                                rows: 3
                                            }}
                                        >
                                            {item.smallName}
                                        </Typography.Paragraph>
                                        <Typography.Paragraph
                                            ellipsis={{
                                                rows: 1
                                            }}
                                        >
                                            {tagList.map(item => (
                                                <Tag>{item}</Tag>
                                            ))}
                                        </Typography.Paragraph>
                                    </>
                                }/>
                            </Card>
                        </Col>
                    )
                })}
            </Row>
            <Divider/>
        </div>
    )
};