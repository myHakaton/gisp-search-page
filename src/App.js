import './App.css';
import {Button, Layout} from "antd";
import {HomePage} from "./home";
import {Setting} from "./setting";
import {useEffect, useState} from "react";
import FingerprintJS from "@fingerprintjs/fingerprintjs";
import {EyeInvisibleOutlined} from '@ant-design/icons';
import axios from "axios";

function App() {
    const [visitorId, setVisitorId] = useState();
    const [user, setUser] = useState();
    useEffect(() => {
        FingerprintJS.load().then(fp => {
            return fp.get()
        }).then(result => {
            return axios.post('/api/getUser', {
                fingerprint: result.visitorId
            }).then(response => {
                setVisitorId(response.data.fingerprint);
                setUser(response.data);
            });
        });
    }, []);
    return (
        <Layout className="layout">
            <Layout.Header className="header">
                <div>
                    <img style={{height: 33, marginRight: 40}}
                         src={'https://gisp.gov.ru/gisplk/assets/images/gisp_logo.png'}/>
                    <img style={{height: 33}}
                         src={'https://gisp.gov.ru/gisplk/assets/images/mptr_logo.png'}/>
                </div>
                <Button icon={<EyeInvisibleOutlined/>} onClick={async () => {
                    const userId = visitorId;
                    setVisitorId(undefined);
                    await axios.post('/api/reset', {
                        fingerprint: visitorId
                    });
                    const {data} = await axios.post('/api/getUser', {
                        fingerprint: userId
                    });
                    setUser(data);
                    setVisitorId(userId);
                }}/>
            </Layout.Header>
            <Layout.Content>
                <Layout>
                    <Layout.Content className="content">
                        <HomePage visitorId={visitorId}  setVisitorId={setVisitorId} user={user} setUser={setUser}/>
                    </Layout.Content>
                    <Setting user={user} setUser={setUser} visitorId={visitorId} setVisitorId={setVisitorId}/>
                </Layout>
            </Layout.Content>
        </Layout>
    );
}

export default App;
